﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BotTravinan.Travian.Batiments
{
    public class Batiment
    {
        public string Xpath { get; set; } = "";
        public string Name { get; set; } = "";
        public string Level { get; set; } = "";

        /*
        public Detail(string name) {
            Name = name;

        }
        */
    }

}