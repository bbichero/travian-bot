﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BotTravinan.Travian
{
    public class Bot
    {
        public Bot()
        {
            Champ = new Champ();
            Ressources = new Ressources();
            CentreDuVillage = new CentreDuVillage();
            Carte = new Carte();
        }

        public Ressources Ressources { get; set; }
        public Champ Champ { get; set; }
        public CentreDuVillage CentreDuVillage { get; set; }
        public Carte Carte { get; set; }
    }
}
