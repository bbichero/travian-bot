﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*
 * List current ressources available
 * Depot, bois, argile, fer, cereale et cilot
 */
namespace BotTravinan.Travian
{
    public class Ressources : List<Ressource>
    {

    }

    public class Ressource
    {
        public string Type { get; set; } = "";

        public string Xpath { get; set; } = "";
        public string Id { get; set; } = "";
        public string Value { get; set; } = "";

        public string XpathMax { get; set; } = "";
        public string IdMax { get; set; } = "";
        public string ValueMax { get; set; } = "";

        public static Bot InitRessources(string Url, ChromeDriver driver, Bot Bot)
        {
            string[] Type = { "Bois", "Terre", "Fer", "Cereales"};
            string[] Id = { "l1", "l2", "l3", "l4"};
            string[] IdMax = { "stockBarWarehouse", "stockBarWarehouse", "stockBarWarehouse", "stockBarGranary" };

            for (int i = 0; i < Id.Length; i++)
            {
                Ressource Ressource = new Ressource();
                IWebElement Value = driver.FindElement(By.Id(Id[i]));
                IWebElement ValueMax = driver.FindElement(By.Id(IdMax[i]));
                Ressource.Type = Type[i];
                Ressource.Id = Id[i];
                Ressource.Value = Value.Text;
                Ressource.IdMax = IdMax[i];
                Ressource.ValueMax = ValueMax.Text;
                Bot.Ressources.Add(Ressource);
            }

            return Bot;
        }

    }
}